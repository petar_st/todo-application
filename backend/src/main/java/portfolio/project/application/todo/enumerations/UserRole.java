package portfolio.project.application.todo.enumerations;

public enum UserRole {
    ADMIN,
    USER
}
