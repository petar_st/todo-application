package portfolio.project.application.todo.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private LocalDate dueDate;

    @Column
    private LocalTime time;

    @Column
    private Boolean completed;

    @ManyToOne
    private Project project;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "task_labels", joinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "labels_id", referencedColumnName = "id")})
    private List<Label> labels;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Subtask> subtask;

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public List<Subtask> getSubtasks() {
        return subtask;
    }

    public void setSubtasks(List<Subtask> subtasks) {
        this.subtask = subtasks;
    }

    public List<Subtask> getSubtask() {
        return subtask;
    }

    public void setSubtask(List<Subtask> subtask) {
        this.subtask = subtask;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", date=" + dueDate +
                ", time=" + time +
                ", labels=" + labels +
                ", subtasks=" + subtask +
                '}';
    }
}
