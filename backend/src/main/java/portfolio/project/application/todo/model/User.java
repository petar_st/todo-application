package portfolio.project.application.todo.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import portfolio.project.application.todo.enumerations.UserRole;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column
    private String email;

    @Column
    private String password;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @OneToMany(mappedBy = "projectOwner")
    private List<Project> projectOwnerList;

    @ManyToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Project> project;

    @OneToMany(mappedBy = "user")
    private List<Label> label;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    public User() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Project> getProjectOwnerList() {
        return projectOwnerList;
    }

    public void setProjectOwnerList(List<Project> projectOwnerList) {
        this.projectOwnerList = projectOwnerList;
    }

    public List<Project> getProjects() {
        return project;
    }

    public void setProject(List<Project> project) {
        this.project = project;
    }

    public List<Label> getLabel() {
        return label;
    }

    public void setLabel(List<Label> label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "User{" +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
