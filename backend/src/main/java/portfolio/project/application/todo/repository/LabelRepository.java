package portfolio.project.application.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.model.Project;

import java.util.List;

@Repository
public interface LabelRepository extends JpaRepository<Label, Long> {
    List<Label> findLabelsByUserId(Long id);
    Label findOneById(Long id);
}
