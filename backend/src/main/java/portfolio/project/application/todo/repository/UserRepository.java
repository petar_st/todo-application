package portfolio.project.application.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import portfolio.project.application.todo.model.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository <User, Long> {
    User findOneById(Long id);
    Optional<User> findFirstByUsername(String username);
    Optional<User> findFirstByUsernameAndPassword(String username,String password);
}
