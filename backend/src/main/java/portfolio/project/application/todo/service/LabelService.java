package portfolio.project.application.todo.service;

import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.model.Project;

import java.util.List;

public interface LabelService {
    List<Label> getAll();

    Label findOneById(Long id);
    List<Label> getAllLabelsFromUser(Long id);

    Label createNewLabel(String labelName, String username);
}
