package portfolio.project.application.todo.service;

import portfolio.project.application.todo.model.Project;

import java.util.List;

public interface ProjectService {
    List<Project> getAll();
    Project getOneById(Long id);
    List<Project> getAllProjectsFromUser(Long id);
    Project create(Project project);
    Project findOneById(Long id);
    Project update(Project project);

    Project createNewProject(String projectName, String username);
}
