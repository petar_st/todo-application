package portfolio.project.application.todo.service;

import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.model.Task;
import portfolio.project.application.todo.web.dto.TaskDTO;

import java.util.List;

public interface TaskService {
    List<Task> getAll();

    Task findById(Long id);
    Task completeTask(Long taskId, String username);

    Task createTask(Task task);

    int deleteTask(Long id, String username);

}
