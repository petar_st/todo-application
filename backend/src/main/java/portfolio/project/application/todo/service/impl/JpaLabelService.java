package portfolio.project.application.todo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.repository.LabelRepository;
import portfolio.project.application.todo.service.LabelService;
import portfolio.project.application.todo.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class JpaLabelService implements LabelService {

    @Autowired
    private LabelRepository labelRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Label> getAll() {
        return labelRepository.findAll();
    }

    @Override
    public Label findOneById(Long id) {
        return labelRepository.findOneById(id);
    }

    @Override
    public List<Label> getAllLabelsFromUser(Long id) {
        return labelRepository.findLabelsByUserId(id);
    }

    @Override
    public Label createNewLabel(String labelName, String username) {
        Optional<User> user = userService.findByUsername(username);

        if (user.isPresent()) {
            User foundUser = user.get();

            Label label = new Label();
            label.setName(labelName);
            label.setUser(foundUser);
            return labelRepository.save(label);
        }

        return null;
    }
}
