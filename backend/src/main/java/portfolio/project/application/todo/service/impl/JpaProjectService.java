package portfolio.project.application.todo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.repository.ProjectRepository;
import portfolio.project.application.todo.service.ProjectService;
import portfolio.project.application.todo.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JpaProjectService implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project getOneById(Long id) {
        return projectRepository.findOneById(id);
    }

    @Override
    public List<Project> getAllProjectsFromUser(Long id) {
    return projectRepository.findProjectsByUserId(id);
    }

    @Override
    public Project create(Project project) {

        System.out.println(project.toString());

        return projectRepository.save(project);
    }

    @Override
    public Project findOneById(Long id) {
        return projectRepository.findOneById(id);
    }

    @Override
    public Project update(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public Project createNewProject(String projectName, String username) {
        Optional<User> user = userService.findByUsername(username);

        if (user.isPresent()) {
            User foundUser = user.get();
            List<User> addUser = new ArrayList<>();
            addUser.add(foundUser);

            Project newProject = new Project();
            newProject.setProjectOwner(foundUser);
            newProject.setUser(addUser);
            newProject.setName(projectName);

            return projectRepository.save(newProject);
        }

        return null;
    }


}
