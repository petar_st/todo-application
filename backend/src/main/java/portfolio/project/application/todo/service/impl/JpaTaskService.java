package portfolio.project.application.todo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import portfolio.project.application.todo.model.Task;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.repository.TaskRepository;
import portfolio.project.application.todo.service.TaskService;
import portfolio.project.application.todo.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class JpaTaskService implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findById(Long id) {
        return taskRepository.findOneById(id);
    }

    @Override
    public Task completeTask(Long taskId, String username) {
        Optional<User> user = userService.findByUsername(username);

        if (user.isPresent()) {
            User foundUser = user.get();

            Task task = taskRepository.findOneById(taskId);
            System.out.println(task);

            task.setCompleted(true);
            return taskRepository.save(task);
        }
        return null;
    }

    @Override
    public Task createTask(Task task) {
        task.setCompleted(false);
        return taskRepository.save(task);

    }

    @Override
    public int deleteTask(Long id, String username) {
        Optional<User> user = userService.findByUsername(username);

        if (user.isPresent()) {
            taskRepository.deleteById(id);
            return 1;
        }
        return 0;
    }

}
