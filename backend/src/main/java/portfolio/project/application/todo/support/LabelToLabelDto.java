package portfolio.project.application.todo.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.web.dto.LabelDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class LabelToLabelDto implements Converter<Label, LabelDTO> {

    @Override
    public LabelDTO convert(Label label) {
        LabelDTO labelDTO = new LabelDTO();

        labelDTO.setId(label.getId());
        labelDTO.setName(label.getName());
        labelDTO.setUsername(label.getUser().getUsername());

        return labelDTO;
    }

    public List<LabelDTO> convertList(List<Label> labels) {
        List<LabelDTO> labelDTOs = new ArrayList<>();

        for (Label l : labels) {
            labelDTOs.add(convert(l));
        }

        return labelDTOs;

    }
}
