package portfolio.project.application.todo.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.service.ProjectService;
import portfolio.project.application.todo.service.UserService;
import portfolio.project.application.todo.web.dto.ProjectDTO;

@Component
public class ProjectDtoToProject implements Converter<ProjectDTO, Project> {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @Override
    public Project convert(ProjectDTO projectDTO) {
        Project project;

        if (projectDTO.getId() == null) {
            project = new Project();
        } else {
            project = projectService.findOneById(projectDTO.getId());
        }

        project.setName(projectDTO.getName());
        project.setProjectOwner(userService.findOne(projectDTO.getProjectOwnerId()));

        return project;
    }
}
