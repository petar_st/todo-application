package portfolio.project.application.todo.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.web.dto.ProjectDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectToProjectDto implements Converter<Project, ProjectDTO> {

    @Autowired
    private TaskToTaskDto toTaskDto;



    @Override
    public ProjectDTO convert(Project project) {
        ProjectDTO projectDTO = new ProjectDTO();

        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setProjectOwnerUsername(project.getProjectOwner().getUsername());

        if(project.getTasksList() != null) {
            projectDTO.setTasksList(toTaskDto.convertList(project.getTasksList()));
        }



        return projectDTO;
    }

    public List<ProjectDTO> convertList(List<Project> projectList) {
        List<ProjectDTO> projectDtoList = new ArrayList<>();

        for (Project project: projectList) {
            projectDtoList.add(convert(project));
        }

        return projectDtoList;
    }
}
