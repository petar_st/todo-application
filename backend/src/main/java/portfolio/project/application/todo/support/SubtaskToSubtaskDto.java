package portfolio.project.application.todo.support;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.Subtask;
import portfolio.project.application.todo.web.dto.SubtaskDTO;

import java.util.ArrayList;
import java.util.List;


@Component
public class SubtaskToSubtaskDto implements Converter<Subtask, SubtaskDTO> {

    @Autowired
    private TaskToTaskDto toTaskDto;

    @Autowired
    private LabelToLabelDto toLabelDto;

    @Override
    public SubtaskDTO convert(Subtask subtask) {
        SubtaskDTO subtaskDTO = new SubtaskDTO();

        subtaskDTO.setId(subtask.getId());
        subtaskDTO.setTitle(subtask.getTitle());
        subtaskDTO.setDescription(subtask.getDescription());
        subtaskDTO.setDate(subtask.getDate());
//        subtaskDTO.setTask(toTaskDto.convert(subtask.getTask()));
        subtaskDTO.setLabels(toLabelDto.convertList(subtask.getLabels()));
        subtaskDTO.setTime(subtask.getTime());
        subtaskDTO.setCompleted(subtask.getCompleted());
        return subtaskDTO;
    }

    public List<SubtaskDTO> convertList (List<Subtask> subtasks) {
        List<SubtaskDTO> subtaskDTOS = new ArrayList<>();

        for (int i = 0; i < subtasks.size(); i++) {
            subtaskDTOS.add(convert(subtasks.get(i)));
        }

        return subtaskDTOS;
    }
}
