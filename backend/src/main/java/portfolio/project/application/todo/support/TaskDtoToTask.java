package portfolio.project.application.todo.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.model.Task;
import portfolio.project.application.todo.service.LabelService;
import portfolio.project.application.todo.service.ProjectService;
import portfolio.project.application.todo.service.TaskService;
import portfolio.project.application.todo.web.dto.TaskDTO;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class TaskDtoToTask implements Converter<TaskDTO, Task> {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private LabelService labelService;

    @Override
    public Task convert(TaskDTO taskDTO) {
        Task task;

        if (taskDTO.getId() != null) {
            task = taskService.findById(taskDTO.getId());
        } else {
            task = new Task();
        }

        task.setTitle(taskDTO.getTitle());

        if (taskDTO.getDescription() != null) {
            task.setDescription(taskDTO.getDescription());
        }

        if (taskDTO.getDate() != null && taskDTO.getDate() != "") {
            task.setDueDate(LocalDate.parse(taskDTO.getDate()));
        }

        if (taskDTO.getTime() != null && taskDTO.getTime() != "") {
            task.setTime(LocalTime.parse(taskDTO.getTime()));
        }

        if (taskDTO.getCompleted() != null) {
            task.setCompleted(taskDTO.getCompleted());
        }

        Project project = projectService.findOneById(taskDTO.getProjectId());
        task.setProject(project);

        // This will need improvements because a Task can contain multiple Labels
        if (taskDTO.getLabelId() != null) {
            List<Label> labels = new ArrayList<>();
            Label label = labelService.findOneById(taskDTO.getLabelId());
            labels.add(label);

            task.setLabels(labels);
        }

        return task;
    }
}
