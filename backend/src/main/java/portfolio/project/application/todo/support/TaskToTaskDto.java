package portfolio.project.application.todo.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.model.Task;
import portfolio.project.application.todo.web.dto.LabelDTO;
import portfolio.project.application.todo.web.dto.SubtaskDTO;
import portfolio.project.application.todo.web.dto.TaskDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class TaskToTaskDto implements Converter<Task, TaskDTO> {

    @Autowired
    private LabelToLabelDto toLabelDto;

    @Autowired
    private SubtaskToSubtaskDto toSubtaskDto;

    @Override
    public TaskDTO convert(Task task) {
        TaskDTO taskDTO = new TaskDTO();

        taskDTO.setId(task.getId());
        taskDTO.setTitle(task.getTitle());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setProjectId(task.getProject().getId());

        if (task.getTime() != null) {
            taskDTO.setTime(task.getTime().toString());
        }

        if (task.getDueDate() != null) {
            taskDTO.setDate(task.getDueDate().toString());
        }

        if (task.getLabels() != null) {
            List<LabelDTO> labelsDTO = new ArrayList<>();
            for (Label l : task.getLabels()) {
                labelsDTO.add(toLabelDto.convert(l));
            }
            taskDTO.setLabels(labelsDTO);
        }

        if (task.getSubtask() != null) {
            taskDTO.setSubtasks(toSubtaskDto.convertList(task.getSubtasks()));
        }

        if (task.getCompleted() != null) {
            taskDTO.setCompleted(task.getCompleted());
        }


        return taskDTO;
    }

    public List<TaskDTO> convertList(List<Task> tasks) {
        List<TaskDTO> taskDTOs = new ArrayList<>();

        for (Task t : tasks) {
            taskDTOs.add(convert(t));
        }

        return taskDTOs;
    }
}
