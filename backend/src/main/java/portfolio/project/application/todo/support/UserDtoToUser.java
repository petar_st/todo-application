package portfolio.project.application.todo.support;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.web.dto.UserDTO;

@Component
public class UserDtoToUser implements Converter<UserDTO, User> {

    @Override
    public User convert(UserDTO userDto) {
        User user = new User();

        user.setId(userDto.getId());
        user.setUsername(userDto.getUsername());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());

        return user;
    }
}
