package portfolio.project.application.todo.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.web.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;


@Component
public class UserToUserDto implements Converter<User, UserDTO> {

    @Autowired
    private ProjectToProjectDto toProjectDto;

    @Autowired
    private LabelToLabelDto toLabelDto;


    @Override
    public UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setProjectDTOs(toProjectDto.convertList(user.getProjects()));
        userDTO.setLabels(toLabelDto.convertList(user.getLabel()));

        return userDTO;
    }

    public List<UserDTO> convert (List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();

        for (User user: users) {
            userDTOs.add(convert(user));
        }

        return userDTOs;

    }
}
