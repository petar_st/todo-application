package portfolio.project.application.todo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import portfolio.project.application.todo.model.Label;
import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.service.LabelService;
import portfolio.project.application.todo.support.LabelToLabelDto;
import portfolio.project.application.todo.web.dto.LabelDTO;
import portfolio.project.application.todo.web.dto.ProjectDTO;

import java.util.List;


@RestController
@RequestMapping(value = "/api/labels", produces = MediaType.APPLICATION_JSON_VALUE)
public class LabelController {

    @Autowired
    private LabelToLabelDto toLabelDto;

    @Autowired
    private LabelService labelService;

    @GetMapping
    private ResponseEntity<List<LabelDTO>> getAll() {
        List<Label> labelList = labelService.getAll();

        return new ResponseEntity<>(toLabelDto.convertList(labelList), HttpStatus.OK);
    }

    @GetMapping(value = "/create")
    private ResponseEntity<LabelDTO> createProject(@RequestParam String labelName, @RequestParam String username) {
        Label label = labelService.createNewLabel(labelName, username);

        return new ResponseEntity<>(toLabelDto.convert(label), HttpStatus.OK);
    }

//    @GetMapping(value = "/user/{id}")
//    private ResponseEntity<List<LabelDTO>> getProjectsFromUser(@PathVariable Long id) {
//        List<Label> labelList = labelService.getAllLabelsFromUser(id);
//
//        return new ResponseEntity<>(toLabelDto.convertList(labelList), HttpStatus.OK);
//    }

}
