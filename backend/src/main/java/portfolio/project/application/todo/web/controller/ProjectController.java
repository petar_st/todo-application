package portfolio.project.application.todo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import portfolio.project.application.todo.model.Project;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.service.ProjectService;
import portfolio.project.application.todo.service.UserService;
import portfolio.project.application.todo.support.ProjectDtoToProject;
import portfolio.project.application.todo.support.ProjectToProjectDto;
import portfolio.project.application.todo.web.dto.ProjectDTO;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectToProjectDto toProjectDto;

    @Autowired
    private ProjectDtoToProject toProject;

    @GetMapping()
    private ResponseEntity<List<ProjectDTO>> getProjectsFromUser(@RequestParam String username) {
        Optional<User> user = userService.findByUsername(username);

        if (user.isPresent()) {
            List<Project> userProjects = projectService.getAllProjectsFromUser(user.get().getId());
            return  new ResponseEntity<>(toProjectDto.convertList(userProjects), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/create")
    private ResponseEntity<ProjectDTO> createProject(@RequestParam String projectName, @RequestParam String username) {
        Project project = projectService.createNewProject(projectName, username);

        System.out.println(project.toString());

        return new ResponseEntity<>(toProjectDto.convert(project), HttpStatus.OK);
    }

//    @GetMapping(value = "/{id}")
//    private ResponseEntity<ProjectDTO> getProjectById(@PathVariable Long id) {
//        Project project = projectService.getOneById(id);
//        return new ResponseEntity<>(toProjectDto.convert(project), HttpStatus.OK);
//    }

//    @GetMapping(value = "/user/{id}")
//    private ResponseEntity<List<ProjectDTO>> getProjectsFromUser(@PathVariable Long id) {
//        List<Project> projectList = projectService.getAllProjectsFromUser(id);
//
//        return new ResponseEntity<>(toProjectDto.convertList(projectList), HttpStatus.OK);
//    }






}
