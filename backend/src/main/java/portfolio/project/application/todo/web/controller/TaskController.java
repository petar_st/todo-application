package portfolio.project.application.todo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import portfolio.project.application.todo.model.Task;
import portfolio.project.application.todo.service.TaskService;
import portfolio.project.application.todo.support.TaskDtoToTask;
import portfolio.project.application.todo.support.TaskToTaskDto;
import portfolio.project.application.todo.web.dto.TaskDTO;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskToTaskDto toTaskDto;

    @Autowired
    private TaskDtoToTask toTask;

    @GetMapping
    private ResponseEntity<List<TaskDTO>> getAll() {
        List<Task> taskList = taskService.getAll();

        return new ResponseEntity<>(toTaskDto.convertList(taskList), HttpStatus.OK);
    }

    @GetMapping(value = "/complete")
    private ResponseEntity<TaskDTO> completeTask(@RequestParam Long taskId, @RequestParam String username) {
        Task task = taskService.completeTask(taskId, username);

        if (task != null) {
            return new ResponseEntity<>(toTaskDto.convert(task), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    // This needs to be improved, so it has a user-validation because a task could be created via Postman
    @PostMapping(value = "/create")
    private ResponseEntity<TaskDTO> createTask(@RequestBody TaskDTO taskDTO) {
        Task task = taskService.createTask(toTask.convert(taskDTO));

        if (task == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(toTaskDto.convert(task), HttpStatus.OK);
    }
    @DeleteMapping(value = "/delete/{id}")
    private ResponseEntity deleteTask(@PathVariable Long id, @RequestParam String username) {
        int result = taskService.deleteTask(id, username);

        if (result == 1) {
            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }


}
