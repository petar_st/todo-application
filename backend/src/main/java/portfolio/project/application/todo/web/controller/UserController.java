package portfolio.project.application.todo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import portfolio.project.application.todo.enumerations.UserRole;
import portfolio.project.application.todo.model.User;
import portfolio.project.application.todo.security.TokenUtils;
import portfolio.project.application.todo.service.UserService;
import portfolio.project.application.todo.support.TaskToTaskDto;
import portfolio.project.application.todo.support.UserDtoToUser;
import portfolio.project.application.todo.support.UserToUserDto;
import portfolio.project.application.todo.web.dto.AuthUserDTO;
import portfolio.project.application.todo.web.dto.UserDTO;
import portfolio.project.application.todo.web.dto.UserRegistrationDTO;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserToUserDto toUserDto;



    @Autowired
    private UserDtoToUser toUser;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PreAuthorize("permitAll()")
    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody @Validated UserRegistrationDTO userRegistrationDTO) {

        if (userRegistrationDTO.getId() != null || !userRegistrationDTO.getPassword().equals(userRegistrationDTO.getRepeatedPassword())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User user = toUser.convert(userRegistrationDTO);

        String encodedPassword = passwordEncoder.encode(userRegistrationDTO.getPassword());
        user.setPassword(encodedPassword);
        user.setUserRole(UserRole.USER);

        userService.save(user);
        return new ResponseEntity<>(toUserDto.convert(user), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> get(@PathVariable Long id) {
        User user = userService.findOne(id);

        if (user != null) {
            return new ResponseEntity<>(toUserDto.convert(user), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // MOST IMPORTANT OF ALL
    @GetMapping
    public ResponseEntity<UserDTO> getUserInformation(@RequestParam String username) {
       Optional<User> user = userService.findByUsername(username);

        if (user.isPresent()) {
            System.out.println("USER: " + user.get().toString());
            System.out.println("PROJECTS: " + user.get().getProjects().toString());
            System.out.println("LABELS: " + user.get().getLabel());

            return new ResponseEntity<>(toUserDto.convert(user.get()), HttpStatus.OK);
        }

        return null;

    }

//    @GetMapping
//    public ResponseEntity<List<UserDTO>> getAll() {
//        List<User> users = userService.findAll();
//
//        return new ResponseEntity<>(toUserDto.convert(users), HttpStatus.OK);
//    }

    @PreAuthorize("permitAll()")
    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public ResponseEntity authenticateUser(@RequestBody AuthUserDTO dto) {

        // Perform the authentication
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        System.out.println(authentication.isAuthenticated());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        try {
            // Reload user details so we can generate token
            UserDetails userDetails = userDetailsService.loadUserByUsername(dto.getUsername());
            return ResponseEntity.ok(tokenUtils.generateToken(userDetails));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
