package portfolio.project.application.todo.web.dto;

import portfolio.project.application.todo.model.User;

public class LabelDTO {

    private Long id;

    private String name;

    private String username;

    public LabelDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
