package portfolio.project.application.todo.web.dto;

import portfolio.project.application.todo.model.Task;

import java.util.List;

public class ProjectDTO {

    private Long id;

    private String name;

    private String projectOwnerUsername;

    private Long projectOwnerId;

    private List<TaskDTO> tasksList;

    public ProjectDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectOwnerUsername() {
        return projectOwnerUsername;
    }

    public void setProjectOwnerUsername(String projectOwnerUsername) {
        this.projectOwnerUsername = projectOwnerUsername;
    }

    public List<TaskDTO> getTasksList() {
        return tasksList;
    }

    public void setTasksList(List<TaskDTO> tasksList) {
        this.tasksList = tasksList;
    }

    public Long getProjectOwnerId() {
        return projectOwnerId;
    }

    public void setProjectOwnerId(Long projectOwnerId) {
        this.projectOwnerId = projectOwnerId;
    }
}
