-- User 1 / admin
INSERT INTO user (first_name, last_name, email, password, user_role, username) VALUES ("Petar", "Stojakovic", "perabkv96@gmail.com", "$2a$10$EZdoJ7zlq9TO0JjvzWMfM.R.F5gohuybg583adbGIkTcQjiupaqaa", "ADMIN", "petar");

INSERT INTO project(name, project_owner_id) VALUES ("Private", 1);
INSERT INTO project(name, project_owner_id) VALUES ("Work", 1);

INSERT INTO label (name, user_id) VALUES ("Todo", 1);
INSERT INTO label (name, user_id) VALUES ("In Progress", 1);
INSERT INTO label (name, user_id) VALUES ("Urgent!", 1);
INSERT INTO label (name, user_id) VALUES ("Paused", 1);

INSERT INTO task (title, description, project_id, completed, due_date, time) VALUES ("Clean the Room", "In the morning do a quick room cleaning", 1, 0, '2022-04-27', '12:00');
INSERT INTO task (title, description, project_id, completed, time) VALUES ("Go to shop", "Get daily groceries", 1, 0, '15:00') ;
INSERT INTO task (title, description, project_id, completed, due_date) VALUES ("Make the invoice", "Create the invoice for project AAA", 2, 0, '2022-04-27' );
INSERT INTO task (title, description, project_id, completed) VALUES ("Talk with HR", "Talk with the HR about the issue from Friday", 2, 0 );
INSERT INTO task (title, description, project_id, completed, due_date) VALUES ("Weekly meeting", "Weekly meeting with Production team", 2, 0 , '2022-04-29') ;

INSERT INTO task_labels (task_id, labels_id) VALUES (1, 1);
INSERT INTO task_labels (task_id, labels_id) VALUES (2, 2);
INSERT INTO task_labels (task_id, labels_id) VALUES (3, 3);
INSERT INTO task_labels (task_id, labels_id) VALUES (4, 1);
INSERT INTO task_labels (task_id, labels_id) VALUES (5, 1);

INSERT INTO project_user (project_id, user_id) VALUES(1, 1);
INSERT INTO project_user (project_id, user_id) VALUES(2, 1);

INSERT INTO subtask (description, title, task_id, completed, date, time) VALUES ('subtask description test1', 'subtask title test1', 1, 0, '2022-04-27', '15:20' );
INSERT INTO subtask (description, title, task_id, completed) VALUES ('subtask description test2', 'subtask title test2', 1, 0 );
INSERT INTO subtask (description, title, task_id, completed) VALUES ('subtask description test3', 's.title test3', 2, 0 );
INSERT INTO subtask (description, title, task_id, completed) VALUES ('subtask description test4', 's.title test4', 2, 0 );
INSERT INTO subtask (description, title, task_id, completed) VALUES ('subtask description test5', 's.title test5', 2, 0 );

INSERT INTO task_subtask (task_id, subtask_id) VALUES (1, 1);
INSERT INTO task_subtask (task_id, subtask_id) VALUES (1, 2);
INSERT INTO task_subtask (task_id, subtask_id) VALUES (2, 3);
INSERT INTO task_subtask (task_id, subtask_id) VALUES (2, 4);
INSERT INTO task_subtask (task_id, subtask_id) VALUES (2, 5);

-- User 2 / user
INSERT INTO user (first_name, last_name, email, password, user_role, username) VALUES ("Dusko", "Bozic", "duskobozic@gmail.com", "$2a$10$5GK/B8vWqFK8ZtVy87.IaeFe3WSqvJ990nzoJeu8BQRJnTuqxEhDe", "USER", "dusko96");
