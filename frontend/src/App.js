import React from "react";
import { HashRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/elements/navbar/Navbar";
import "./App.css";
import Home from "./components/pages/home/Home";
import Login from "./components/pages/login/Login";
import AppHome from "./components/application/home/AppHome";
import Footer from "./components/elements/footer/Footer";
import About from "./components/pages/about/About";

class App extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Navbar />
          <div>
            <Routes>
              <Route exact path="/login" element={<Login />} />
              <Route exact path="/" element={<Home />} />
              <Route exact path="/app" element={<AppHome />} />
              <Route exact path="/about" element={<About />} />
            </Routes>
          </div>
          <Footer />
        </Router>
      </>
    );
  }
}
export default App;
