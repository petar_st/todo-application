import React from "react";
import { Button, FormControl, Table } from "react-bootstrap";
import "./Editor.css";
import SubtaskPopup from "../modal/SubtaskPopup.js";
import Axios from "../../../apis/Axios";
import { configure } from "@testing-library/react";
import Task from "../processing/Task";

class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSubtaskPopup: false,

      project: this.props.project,
      selectedTaskId: "",
    };
  }

  // This function manages if the Subtask popup should be displayed, or not
  toggleSubtaskPopup(e) {
    this.setState({
      showSubtaskPopup: !this.state.showSubtaskPopup,
      selectedTaskId: e.target.value,
    });
  }

  // This function adds labels next to the task title (label example: todo, in progress, urgent, etc.)
  renderLabels(labelList) {
    return labelList.map((label) => {
      return (
        <span className="label">
          <i className="label-text">{label.name}</i>
        </span>
      );
    });
  }

  completeTask(id) {
    let taskId = id;
    let username = window.localStorage["username"];

    let config = {
      params: {
        taskId: taskId,
        username: username,
      },
    };

    Axios.get("/tasks/complete", config)
      .then((res) => {
        // needs improvements
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  deleteTask(id) {
    console.log(id);
    let taskId = id;
    let username = window.localStorage["username"];

    let config = {
      params: {
        taskId: taskId,
        username: username,
      },
    };

    Axios.delete("/tasks/delete/" + taskId, config)
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderTasks() {
    {
      console.log(this.props.project);
    }
    // return this.props.project.tasksList.map((task) => {
    return (
      <Task taskList={this.props.project.tasksList}></Task>
      // <tr>
      //   {!task.completed ? (
      //     <div className="task-tr">
      //       {this.renderLabels(task.labels)}
      //       <span className="title">{task.title}</span>
      //       {task.date ? (
      //         <span className="date-time">
      //           &nbsp;
      //           <FormControl
      //             type="date"
      //             className="date-time-picker"
      //             defaultValue={task.date}
      //           ></FormControl>
      //         </span>
      //       ) : null}
      //       {task.time ? (
      //         <span className="date-time">
      //           &nbsp;
      //           <FormControl
      //             type="time"
      //             className="date-time-picker"
      //             defaultValue={task.time}
      //           ></FormControl>
      //         </span>
      //       ) : null}
      //       <Button
      //         className="action-button"
      //         onClick={() => this.completeTask(task.id)}
      //       >
      //         <i class="fa-regular fa-circle-check"></i>
      //       </Button>
      //       <Button
      //         className="action-button"
      //         onClick={() => this.deleteTask(task.id)}
      //       >
      //         <i class="fa-regular fa-trash-can"></i>
      //       </Button>
      //       {task.subtasks != "" ? (
      //         <td>
      //           <Button
      //             className="view-subtasks-buttton"
      //             type="button"
      //             class="btn btn-primary"
      //             data-toggle="modal"
      //             value={task.id}
      //             onClick={this.toggleSubtaskPopup.bind(this)}
      //           >
      //             view subtasks
      //           </Button>
      //           {this.state.showSubtaskPopup ? (
      //             <SubtaskPopup
      //               selectedTaskId={this.state.selectedTaskId}
      //               project={this.state.project}
      //               closePopup={this.toggleSubtaskPopup.bind(this)}
      //             />
      //           ) : null}
      //         </td>
      //       ) : null}
      //     </div>
      //   ) : (
      //     ""
      //   )}
      // </tr>
    );
  }

  render() {
    return (
      <>
        <Table className="task-table">
          <thead>
            <tr>
              <th className="project-name">
                <i>
                  <u>PROJECT: {this.props.project.name}</u>
                </i>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.props.project.tasksList == null ? null : this.renderTasks()}
          </tbody>
        </Table>
      </>
    );
  }
}

export default Editor;
