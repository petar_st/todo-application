import React from "react";
import "./AppHome.css";
import Axios from "../../../apis/Axios";
import Login from "../../pages/login/Login";
import Toolbar from "../toolbar/Toolbar";
import Sidebar from "../sidebar/Sidebar";
import { Col, Row } from "react-bootstrap";
import Editor from "../editor/Editor";

class AppHome extends React.Component {
  constructor(props) {
    super(props);

    this.state = { projects: [], selectedProject: "", labels: [] };
  }

  componentDidMount() {
    this.getUserInformation();
  }

  // idea behind this: get all the information from the user including
  // list of project, labels, tasks, subtasks, ... all at once and work with that
  getUserInformation = async () => {
    var username = window.localStorage["username"];

    let config = {
      params: {
        username: username,
      },
    };

    await Axios.get("/users", config)
      .then((res) => {
        this.setState({
          projects: res.data.projectDTOs,
          labels: res.data.labels,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // Event that gets created after a different project is selected in the
  // dropdown menu in the Sidebar.js class
  projectSelectorEvent(projectChangedEvenet) {
    var projectId = projectChangedEvenet.target.value;

    for (let i = 0; i < this.state.projects.length; i++) {
      if (projectId == this.state.projects[i].id) {
        this.setState({ selectedProject: this.state.projects[i] });
        return;
      }
    }
  }

  render() {
    return (
      <>
        <div className="tasks-div">
          {window.localStorage["jwt"] ? (
            <Row>
              <Col sm={3} className="sidebar">
                {this.state.projects.length == 0 ? null : (
                  <Sidebar
                    projects={this.state.projects}
                    projectSelectorEvent={this.projectSelectorEvent.bind(this)}
                    labels={this.state.labels}
                  />
                )}
              </Col>
              <Col sm={9}>
                <Toolbar
                  projectId={this.state.selectedProject.id}
                  labels={this.state.labels}
                />
                {this.state.selectedProject != "" ? (
                  <div className="task-section">
                    <Editor project={this.state.selectedProject} />
                  </div>
                ) : (
                  <h1>Choose project!</h1>
                )}
              </Col>
            </Row>
          ) : (
            <Login />
          )}
        </div>
      </>
    );
  }
}

export default AppHome;
