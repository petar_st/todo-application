import React from "react";
import Axios from "../../../apis/Axios";
import { Button, Form, FormControl, Table } from "react-bootstrap";
import "./Popup.css";

class AddLabelPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { labelName: "" };
  }

  labelNameChange(e) {
    this.setState({ labelName: e.target.value });
  }

  createLabel() {
    let labelName = this.state.labelName;
    let username = window.localStorage["username"];

    let config = {
      params: {
        labelName: labelName,
        username: username,
      },
    };

    Axios.get("/labels/create", config)
      .then((res) => {
        console.log(res);
        this.props.closePopup();
        this.props.returnLabel(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <div className="popup">
        <div className="popup_inner text-center">
          <h1 className="modal-title">
            <u>Add a New Label</u>
          </h1>
          <Form.Group className="popup-table">
            <label className="label-title">Label Name</label>

            <FormControl
              className="input-field"
              type="text"
              onChange={(e) => this.labelNameChange(e)}
            ></FormControl>

            <Button
              className="btn btn-success"
              type="submit"
              onClick={() => this.createLabel()}
            >
              Create Label
            </Button>
          </Form.Group>
          <br />
          <Button className="close-button" onClick={this.props.closePopup}>
            Close
          </Button>
        </div>
      </div>
    );
  }
}

export default AddLabelPopup;
