import React from "react";
import Axios from "../../../apis/Axios";
import { Button, Form, FormControl, Table } from "react-bootstrap";
import "./Popup.css";

class AddProjectPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { projectName: "" };
  }

  projectNameChange(e) {
    this.setState({ projectName: e.target.value });
  }

  createProject() {
    let projectName = this.state.projectName;
    let username = window.localStorage["username"];

    let config = {
      params: {
        projectName: projectName,
        username: username,
      },
    };

    Axios.get("/projects/create", config)
      .then((res) => {
        console.log(res);
        this.props.closePopup();
        this.props.returnProject(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <div className="popup">
        <div className="popup_inner text-center">
          <h1 className="modal-title">
            <u>Add a New Project</u>
          </h1>
          <Form.Group className="popup-table">
            <label className="label-title">Project Name</label>

            <FormControl
              className="input-field"
              type="text"
              onChange={(e) => this.projectNameChange(e)}
            ></FormControl>

            <Button
              className="btn btn-success"
              type="submit"
              onClick={() => this.createProject()}
            >
              Create Project
            </Button>
          </Form.Group>
          <br />
          <Button className="close-button" onClick={this.props.closePopup}>
            Close
          </Button>
        </div>
      </div>
    );
  }
}

export default AddProjectPopup;
