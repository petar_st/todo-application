import React from "react";
import Axios from "../../../apis/Axios";
import { Button, Form, FormControl, FormSelect, Table } from "react-bootstrap";
import "./Popup.css";

class AddTaskPopup extends React.Component {
  constructor(props) {
    super(props);

    let params = {
      taskTitle: "",
      taskDescription: "",
      dueDate: "",
      dueTime: "",
      labelId: "",
    };
    this.state = {
      params: params,
      projectId: this.props.projectId,
      labels: this.props.labels,
    };
  }

  componentDidMount() {}

  onInputChange(e) {
    const name = e.target.name;
    const value = e.target.value;

    let params = this.state.params;
    params[name] = value;

    console.log(params);
    this.setState({ params: params });
  }

  createTask() {
    let username = window.localStorage["username"];

    let params = this.state.params;

    if (params.taskTitle == null || params.taskTitle == "") {
      alert("Title cannot be empty");
      return;
    }
    let dto = {
      title: params.taskTitle,
      description: params.taskDescription,
      date: params.dueDate,
      time: params.dueTime,
      projectId: this.state.projectId,
      labelId: params.labelId,
    };

    console.log(JSON.stringify(dto));
    Axios.post("/tasks/create", dto)
      .then((res) => {
        console.log(res);
        this.props.closePopup();
        window.location.reload();
      })
      .catch((err) => {
        alert("Something went wrong. Plesae try again");
        console.log(err);
      });
  }

  // Render Label Dropdown
  renderLabelDropdown() {
    return this.state.labels.map((e) => {
      return (
        <option key={e.id} value={e.id}>
          {e.name}
        </option>
      );
    });
  }

  render() {
    return (
      <div className="popup">
        <div className="popup_inner text-center">
          <h1 className="modal-title">
            <u>Add New Task</u>
          </h1>
          <Form.Group className="popup-table">
            <label className="label-title">Task Title</label>
            <FormControl
              name="taskTitle"
              className="input-field input-task"
              type="text"
              placeholder="Enter Task Title"
              required
              onChange={(e) => this.onInputChange(e)}
            ></FormControl>

            <label className="label-title">Task Description</label>
            <FormControl
              name="taskDescription"
              className="input-field input-task"
              type="text"
              placeholder="Enter Task Description"
              onChange={(e) => this.onInputChange(e)}
            ></FormControl>

            <label className="label-title">Due Date</label>
            <FormControl
              name="dueDate"
              className="input-field input-task"
              type="date"
              onChange={(e) => this.onInputChange(e)}
            ></FormControl>

            <label className="label-title">Due Time</label>
            <FormControl
              name="dueTime"
              className="input-field input-task"
              type="time"
              onChange={(e) => this.onInputChange(e)}
            ></FormControl>

            <label className="label-title">Choose Label</label>
            <FormSelect
              className="input-field input-task input-task-label"
              name="labelId"
              onChange={(e) => this.onInputChange(e)}
            >
              <option key="">Select Label</option>
              {this.renderLabelDropdown()}
            </FormSelect>

            <Button
              className="btn btn-success "
              type="submit"
              onClick={() => this.createTask()}
            >
              Create Task
            </Button>
          </Form.Group>
          <br />
          <Button className="close-button" onClick={this.props.closePopup}>
            Close
          </Button>
        </div>
      </div>
    );
  }
}

export default AddTaskPopup;
