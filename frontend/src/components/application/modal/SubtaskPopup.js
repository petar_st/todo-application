import React from "react";
import { Button, Table } from "react-bootstrap";
import "./Popup.css";

class SubtaskPopup extends React.Component {
  // This function goes over all the project's tasks, and finds the one that
  // should be rendered in the Popup
  findSubtasks() {
    var taskId = this.props.selectedTaskId;
    var allTasks = this.props.project.tasksList;

    for (let i = 0; i < allTasks.length; i++) {
      if (taskId == allTasks[i].id) {
        console.log(allTasks[i].subtasks);
        return allTasks[i].subtasks;
      }
    }
  }

  // Subtasks rendering
  renderSubtasks() {
    var subtasks = this.findSubtasks();

    return subtasks.map((subtask) => {
      return (
        <tr key={subtask.id} className="subtask-tr">
          <td className="popup-table-td">
            <u> {subtask.title}</u>
            {subtask.date != null ? (
              <>
                &nbsp;<i class="fa-regular fa-calendar-days"></i> {subtask.date}
              </>
            ) : null}
            {subtask.time != null ? (
              <>
                &nbsp;<i class="fa-regular fa-clock"></i> {subtask.time}
              </>
            ) : null}
            <br /> {subtask.description}
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="popup">
        <div className="popup_inner text-center">
          {/* <h1 className="modal-title">{this.state.taskTitle}</h1> */}
          <Table className="popup-table">
            <tbody>{this.renderSubtasks()}</tbody>
          </Table>
          <Button className="close-button" onClick={this.props.closePopup}>
            Close
          </Button>
        </div>
      </div>
    );
  }
}

export default SubtaskPopup;
