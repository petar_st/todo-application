import React from "react";
import "./Task.css";
import Axios from "../../../apis/Axios";
import "../editor/Editor.css";
import { Button, FormControl, Table } from "react-bootstrap";
import SubtaskPopup from "../modal/SubtaskPopup.js";

class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      taskList: this.props.taskList,
    };
  }

  toggleSubtaskPopup(e) {
    this.setState({
      showSubtaskPopup: !this.state.showSubtaskPopup,
      selectedTaskId: e.target.value,
    });
  }

  renderLabels(labelList) {
    return labelList.map((label) => {
      return (
        <span className="label">
          <i className="label-text">{label.name}</i>
        </span>
      );
    });
  }

  completeTask(id) {
    console.log(id);
    let taskId = id;
    let username = window.localStorage["username"];

    let config = {
      params: {
        taskId: taskId,
        username: username,
      },
    };

    Axios.get("/tasks/complete", config)
      .then((res) => {
        // needs improvements
        console.log(res);
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  deleteTask(id) {
    console.log(id);
    let taskId = id;
    let username = window.localStorage["username"];

    let config = {
      params: {
        taskId: taskId,
        username: username,
      },
    };

    Axios.delete("/tasks/delete/" + taskId, config)
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  toggleSubtaskPopup(e) {
    this.setState({
      showSubtaskPopup: !this.state.showSubtaskPopup,
      selectedTaskId: e.target.value,
    });
  }

  render() {
    var taskList = this.state.taskList;
    console.log("task.js");
    console.log(taskList);
    return taskList.map((task) => {
      return (
        <>
          {!task.completed ? (
            <div className="task-card">
              <tr className="task-tr">
                <p className="task-title text-center">{task.title}</p>

                <br />
                <span className="task-description">
                  <i>{task.description}</i>
                </span>

                <div className="cta-buttons text-center">
                  {this.renderLabels(task.labels)}
                  <Button
                    className="action-button"
                    onClick={() => this.completeTask(task.id)}
                  >
                    <i class="fa-regular fa-circle-check"></i>
                  </Button>
                  <Button
                    className="action-button"
                    onClick={() => this.deleteTask(task.id)}
                  >
                    <i class="fa-regular fa-trash-can"></i>
                  </Button>
                  <Button className="action-button">
                    <i class="fa-solid fa-list-check"></i>
                  </Button>
                </div>
              </tr>{" "}
            </div>
          ) : null}
        </>
      );
    });
  }
}
export default Task;
