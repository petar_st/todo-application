import React from "react";
import { Button, FormSelect } from "react-bootstrap";
import AddProjectPopup from "../modal/AddProjectPopup";
import AddLabelPopup from "../modal/AddLabelPopup";
import "./Sidebar.css";

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: props.projects,
      labels: props.labels,
      showAddProjectPopup: false,
      showAddLabelPopup: false,
    };
  }

  componentDidMount() {}

  toggleAddProjectPopup(e) {
    this.setState({
      showAddProjectPopup: !this.state.showAddProjectPopup,
    });
  }

  toggleAddLabelPopup(e) {
    this.setState({
      showAddLabelPopup: !this.state.showAddLabelPopup,
    });
  }

  // Parsed as props to AddProjectPopup so after a new Project gets created
  // it will be returned back and added to state. Code is optimized so no      // additional requests are sent to the backend
  addProjectToState(newProject) {
    var projects = this.state.projects;
    projects.push(newProject);

    this.setState({ projects: projects });
  }

  // Parsed as props to AddLabelPopup so after a new Labels gets created
  // it will be returned back and added to state. Code is optimized so no      // additional requests are sent to the backend
  addLabelToState(newLabel) {
    var labels = this.state.labels;
    labels.push(newLabel);
    this.setState({ labels: labels });
  }

  // Render Project Dropdown
  renderProjectDropdown() {
    return this.state.projects.map((e) => {
      return (
        <option key={e.id} value={e.id}>
          {e.name}
        </option>
      );
    });
  }

  // Render Label Dropdown
  renderLabelDropdown() {
    return this.state.labels.map((e) => {
      return (
        <option key={e.id} value={e.id}>
          {e.name}
        </option>
      );
    });
  }

  render() {
    return (
      <>
        <div className="sidebar-container">
          <FormSelect
            className="entity-selector"
            onChange={(e) => this.props.projectSelectorEvent(e)}
          >
            <option key="" selected disabled>
              Select Project
            </option>
            {this.renderProjectDropdown()}
          </FormSelect>
          <Button
            className="add-new-entity"
            onClick={this.toggleAddProjectPopup.bind(this)}
          >
            Add Project
          </Button>
          {this.state.showAddProjectPopup ? (
            <AddProjectPopup
              closePopup={this.toggleAddProjectPopup.bind(this)}
              returnProject={this.addProjectToState.bind(this)}
            ></AddProjectPopup>
          ) : null}

          <FormSelect className="entity-selector">
            <option key="" selected>
              Select Label
            </option>
            {this.renderLabelDropdown()}
          </FormSelect>
          <Button
            className="add-new-entity"
            onClick={this.toggleAddLabelPopup.bind(this)}
          >
            Add Label
          </Button>
          {this.state.showAddLabelPopup ? (
            <AddLabelPopup
              closePopup={this.toggleAddLabelPopup.bind(this)}
              returnLabel={this.addLabelToState.bind(this)}
            ></AddLabelPopup>
          ) : null}
        </div>
      </>
    );
  }
}

export default Sidebar;
