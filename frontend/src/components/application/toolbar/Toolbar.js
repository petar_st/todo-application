import React from "react";
import { Button } from "react-bootstrap";
import AddTaskPopup from "../modal/AddTaskPopup";
import "./Toolbar.css";

class Toolbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showAddTaskPopup: false,
    };
  }

  toggleAddTaskPopup(e) {
    this.setState({
      showAddTaskPopup: !this.state.showAddTaskPopup,
    });
  }

  componentDidUpdate() {
    console.log(this.props);
  }

  render() {
    return (
      <div className="toolbar ">
        <ul className="list-group list-group-horizontal">
          <li className="list-item">
            {this.props.projectId == undefined ? null : (
              <Button onClick={this.toggleAddTaskPopup.bind(this)}>
                <i>task</i>
                <i class="fa-regular fa-plus"></i>
              </Button>
            )}
            {this.state.showAddTaskPopup ? (
              <AddTaskPopup
                projectId={this.props.projectId}
                closePopup={this.toggleAddTaskPopup.bind(this)}
                labels={this.props.labels}
              ></AddTaskPopup>
            ) : null}
          </li>
        </ul>
      </div>
    );
  }
}

export default Toolbar;
