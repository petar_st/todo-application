import React from "react";
import "./Home.css";
import Cards from "./card/Cards";
import "../home/Home.css";

class Home extends React.Component {
  render() {
    return (
      <div>
        <div className="home-container">
          <video
            src="/videos/home-video-trimmed-compressed.mp4"
            className="home-video"
            autoPlay
            loop
            muted
          />
          <h1 className="title">INCREASE PRODUCTIVITY</h1>
          <p className="title">What are you waiting for?</p>
        </div>
        <div className="cards-container">
          <Cards />
        </div>
      </div>
    );
  }
}

export default Home;
