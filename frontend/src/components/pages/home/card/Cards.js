import React from "react";
import "./Cards.css";
import CardItem from "./CardItem";

function Cards() {
  return (
    <div className="cards">
      <h1>Scroll down for more information ⬇</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem
              src="images/card-image-1.svg"
              text="Easily manage your day-to-day tasks and orem Ipsum is simply dummy text."
              label="Personal"
              path="/services"
            />
            <CardItem
              src="images/card-image-2.svg"
              text="Share and collaborate with your team members on tasks or projects"
              label="Team Collaboration"
              path="/services"
            />
          </ul>
          <ul className="cards__items">
            <CardItem
              src="images/card-image-3.svg"
              text="Easily monitor the task progress. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour."
              label="Feature"
              path="/services"
            />
            <CardItem
              src="images/card-image-4.svg"
              text="Mobile Friendly Ecosystem. To generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
              label="iOS/Android"
              path="/services"
            />
            <CardItem
              src="images/card-image-5.svg"
              text="More features to come! Ipsum which looks reasonable. The generated Lorem Ipsum"
              label="Much more"
              path="/services"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
