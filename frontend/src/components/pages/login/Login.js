import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import "./Login.css";
import { login } from "../../../services/Auth";
import "../../../App.css";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = { username: "", password: "" };
  }

  onInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let change = {};
    change[name] = value;
    console.log(change);
    this.setState(change);
  }

  doLogin() {
    login(this.state);
  }

  render() {
    return (
      <Row className="justify-content-center login-form">
        <Col md={8}>
          <Form className="form">
            <Form.Group className="form-group">
              <Form.Label className="form-label">Username</Form.Label>
              <Form.Control
                className="form-control"
                type="text"
                name="username"
                placeholder="username"
                onChange={(e) => this.onInputChange(e)}
              />
            </Form.Group>
            <Form.Group className="form-group">
              <Form.Label className="form-label">Password</Form.Label>
              <Form.Control
                className="form-control"
                type="password"
                name="password"
                placeholder="passowrd"
                onChange={(e) => this.onInputChange(e)}
              />
            </Form.Group>
            <Form.Group className="button-row">
              <Button
                className="login-button"
                variant="success"
                onClick={() => {
                  login(this.state.username, this.state.password);
                }}
              >
                Continue
              </Button>
            </Form.Group>
          </Form>
        </Col>
        <Row className="account-registration">
          <h3 className="account-registration-title">Need an account?</h3>
          <p className="account-registration-content">
            Click on this link to register
          </p>
          <p className="account-registration-content">
            Username: petar passowrd: petar
          </p>
        </Row>
      </Row>
    );
  }
}

export default Login;
